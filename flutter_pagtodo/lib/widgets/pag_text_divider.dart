import 'package:flutter/material.dart';

class PagTextDivider extends StatelessWidget {
  final String text;

  PagTextDivider({@required this.text});

  @override
  Widget build(BuildContext context) => Container(
    width: MediaQuery.of(context).size.width,
    margin: const EdgeInsets.only(left: 30.0, right: 30.0, top: 20.0),
    alignment: Alignment.center,
    child: Row(
      children: <Widget>[
        new Expanded(
          child: new Container(
            margin: EdgeInsets.all(8.0),
            decoration: BoxDecoration(border: Border.all(width: 0.25, color: Colors.grey.shade400)),
          ),
        ),
        Text(
          text,
          style: TextStyle(
            color: Colors.grey.shade400,
            fontWeight: FontWeight.bold,
          ),
        ),
        new Expanded(
          child: new Container(
            margin: EdgeInsets.all(8.0),
            decoration: BoxDecoration(border: Border.all(width: 0.25, color: Colors.grey.shade400)),
          ),
        ),
      ],
    ),
  );
}
