import 'package:flutter/material.dart';

class InputFieldLabel extends StatelessWidget {
  final String labelTitle;

  InputFieldLabel({this.labelTitle});

  @override
  Widget build(BuildContext context) {

    return new Row(
      children: <Widget>[
        new Expanded(
          child: new Padding(
            padding: const EdgeInsets.only(left: 40.0),
            child: new Text(
              labelTitle.toUpperCase(),
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Color(0xff002c47),
                fontSize: 15.0,
              ),
            ),
          ),
        ),
      ],
    );
  }
}