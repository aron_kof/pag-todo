import 'package:flutter/material.dart';

/* This widget will serve as utility
similar to HOCs. It will enable onInit
hook for stateless widgets via a statefull wrapper */

class InitWrapper extends StatefulWidget {
  final Function onInit;
  final Widget child;

  const InitWrapper({@required this.onInit, @required this.child});
  @override
  _InitWrapperState createState() => _InitWrapperState();
}

class _InitWrapperState extends State<InitWrapper> {
@override
  void initState() {
    if(widget.onInit != null) {
      widget.onInit();
    }
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return widget.child;
  }
}