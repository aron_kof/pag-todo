import 'package:flutter/material.dart';

class PagDivider extends StatelessWidget {
  final double height;

  PagDivider({this.height});

  @override
  Widget build(BuildContext context) => Container(height: height ?? 24.0);
}