import 'package:flutter/material.dart';

class PagTextField extends StatelessWidget {
  final String hintText;
  final bool isPassword;
  final bool autofocus;

  final Function onChangedFunction;
  final Function validateFunction;
  final Function saveFunction;

  PagTextField({
    this.hintText,
    this.isPassword,
    this.autofocus = false,
    this.onChangedFunction,
    this.validateFunction,
    this.saveFunction
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      margin: const EdgeInsets.only(left: 40.0, right: 40.0, top: 10.0),
      alignment: Alignment.center,
      padding: const EdgeInsets.only(left: 0.0, right: 10.0),
      child: new Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          new Expanded(
            child: TextFormField(
              autofocus: autofocus,
              obscureText: isPassword ?? false,
              textAlign: TextAlign.left,
              decoration: InputDecoration(
                hintText: hintText,
                hintStyle: TextStyle(color: Colors.grey),

                focusedBorder: const UnderlineInputBorder(
                  borderSide: const BorderSide(color: Color(0xff63d2ff)),
                ),

                errorBorder: const UnderlineInputBorder(
                  borderSide: const BorderSide(color: Colors.redAccent, width: 1.0),
                ),
              ),
              onChanged: onChangedFunction ?? null,
              validator: validateFunction ?? null,
              onSaved: saveFunction ?? null,
            ),
          ),
        ],
      ),
    );
  }
}