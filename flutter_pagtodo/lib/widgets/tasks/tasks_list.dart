import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../models/task.dart';
import './task_tile.dart';
import '../../services/task_service.dart';
import '../../services/animation_service.dart';

class TasksList extends StatefulWidget {
  @override
  TasksListState createState() {
    return new TasksListState();
  }
}

class TasksListState extends State<TasksList> {
  final TasksAnimation _animations = TasksAnimation();

  void _deleteTask(Task task, int index, Animation<double> animation, TaskService taskService) {
    _animations.taskList.currentState.removeItem(index, (context, animation) {
      return SizeTransition(
        sizeFactor: animation,
        child: TaskTile(
          taskTitle: task.name,
          isChecked: task.completed
        )
      );
    });

    taskService.deleteTask(task, index);
  }



  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<TaskService>(
      builder: (context, taskService, child) {
        return AnimatedList(
          key: _animations.taskList,
          initialItemCount: 0,
          itemBuilder: (context, index, animation) {
            final task = taskService.tasks[index];
            return SizeTransition(
              sizeFactor: animation,
              child: TaskTile(
                taskTitle: task.name,
                isChecked: task.completed,
                checkboxCallback: (checkboxState) {
                  taskService.updateTask(task);
                },
                longPressCallback: () {
                  _deleteTask(task, index, animation, taskService);
                },
              ),
            );
          },
        );
      },
    );
  }
}
