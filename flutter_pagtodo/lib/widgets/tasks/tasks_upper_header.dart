import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:provider/provider.dart';
import '../pag_divider.dart';
import '../../gql_operations/user_operations.dart';
import '../../services/routes.dart';
import '../../services/session_service.dart';
import '../../services/task_service.dart';

class TasksUpperHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final UserMutations userMutations = UserMutations();
    final Routes routes = Routes();
    final PagSession _session = PagSession();

    return Mutation(
      options: MutationOptions(
        documentNode: gql(userMutations.signOutMutation),
        update: (Cache cache, QueryResult res) {
          cache.reset();
          _session.clearCookie();

          Navigator.popUntil(context, ModalRoute.withName(routes.login));

          Provider.of<TaskService>(context).resetState();
        },
      ),
      builder: (RunMutation runMutation, QueryResult queryResult) => Container(
        padding: EdgeInsets.only(top: 60.0, left: 30.0, right: 30.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            RawMaterialButton(
              onPressed: () {
                runMutation({});
              },
              constraints: BoxConstraints.tightFor(width: 65.00, height: 65.00),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    Icons.arrow_back,
                    size: 20.0,
                    color: Color(0xff002c47),
                  ),
                  Text('logout',
                    style: TextStyle(
                      fontSize: 10.0,
                      color: Color(0xff002c47),
                    ),
                  )
                ],
              ),
              shape: new CircleBorder(),
              elevation: 2.0,
              fillColor: Colors.white,
              padding: const EdgeInsets.all(15.0),
            ),
            PagDivider(height: 5.0,),
          ] 
        ),
      ),
    );
  }
}