
import '../../services/task_service.dart';
import 'package:provider/provider.dart';
import 'package:flutter/material.dart';
import '../pag_divider.dart';

class TasksLowerHeader extends StatelessWidget {
  final String username;
  final int taskCount;

  TasksLowerHeader({this.username, this.taskCount});

  @override
  Widget build(BuildContext context) {
    final bool initialized = Provider.of<TaskService>(context).initialized;

    return AnimatedContainer(
      duration: Duration(milliseconds: 500),
      padding: EdgeInsets.only(left: 30.0, right: 30.0, bottom: 30.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          PagDivider(height: 10.0,),
          AnimatedDefaultTextStyle(
            duration: Duration(milliseconds: 250),
            style: TextStyle(
              color: initialized ? Colors.white : Color(0xff002c47),
              fontSize: initialized ? 25.0 : 0,
              fontWeight: FontWeight.w700
            ),
            child: Text('welcome ${username ?? 'Username'}'),
          ),
          AnimatedDefaultTextStyle(
            duration: Duration(milliseconds: 350),
            style: TextStyle(
              color: initialized ? Colors.white : Color(0xff002c47),
              fontSize: initialized ? 12.0 : 0,
              fontStyle: FontStyle.italic
            ),
            child: Text(
              taskCount > 0
              ? 'You have $taskCount ${taskCount > 1 ? 'tasks' : 'task'}, get it done'
              : 'You have no tasks yet!',
            ),
          ),
        ],
      ),
    );
  }
}