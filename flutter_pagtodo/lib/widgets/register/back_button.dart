import 'package:flutter/material.dart';

class PagBackButton extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      onPressed: () {
        Navigator.pop(context);
      },
      constraints: BoxConstraints.tightFor(width: 65.00, height: 65.00),
      child: Icon(
        Icons.arrow_back,
        size: 30.0,
        color: Colors.white,
      ),
      shape: new CircleBorder(),
      elevation: 2.0,
      fillColor: Color(0xff002c47),
      padding: const EdgeInsets.all(15.0),
    );
  }
}