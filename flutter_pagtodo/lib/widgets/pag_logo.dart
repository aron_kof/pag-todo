import 'package:flutter/material.dart';

class PagLogo extends StatelessWidget {
  final double marginTop;
  final double marginBottom;

  PagLogo({this.marginTop, this.marginBottom});

  @override
  Widget build(BuildContext context) {

    return Container(
      width: 150.0,
      height: 150.0,
      margin: EdgeInsets.only(top: marginTop ?? 100.0, bottom: marginBottom ?? 0.0),
      decoration: BoxDecoration(
        image: new DecorationImage(
            fit: BoxFit.fill,
            image: (
                AssetImage('assets/pag_logo.png')
            )
        ),
      ),
    );
  }
}