import 'package:flutter/material.dart';
import 'package:flutter_pagtodo/services/routes.dart';
import 'package:flutter_pagtodo/services/user_service.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import '../gql_operations/user_operations.dart';
import 'package:provider/provider.dart';
import '../services/validations_service.dart';

/* ui widgets import */
import '../widgets/input_field_label.dart';
import '../widgets/pag_text_field.dart';
import '../widgets/pag_divider.dart';

/* specific widgets import */
import '../widgets/register/register_button.dart';
import '../widgets/register/back_button.dart';

class SignupScreen extends StatefulWidget {
  @override
  SignupScreenState createState() => new SignupScreenState();
}

class SignupScreenState extends State<SignupScreen> {
  final _formKey = new GlobalKey<FormState>();
  final _validationsService = ValidationsService();

  String _email;
  String _password;
  String _username;
  String _confirmPassword;

  @override
  Widget build(BuildContext context) {
    Routes routes = Routes();
    UserMutations userMutations = UserMutations();

    SnackBar createSnackBar([String optionalMessage]) => SnackBar(
          content: Text(
              optionalMessage ??
                  'Oops, something went wrong, try again please.',
              textAlign: TextAlign.center),
        );

    void displaySnackbarError(BuildContext context, [String optionalMessage]) {
      Scaffold.of(context).showSnackBar(createSnackBar(optionalMessage));
    }

    void errorDialog(BuildContext context, [String errorMessage]) => showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Error'),
              content: Text(errorMessage ??
                  'Oops, something went wrong, try again please.'),
              actions: [
                FlatButton(
                    child: Text("OK"),
                    onPressed: () {
                      Navigator.pop(context);
                    })
              ],
            );
          },
        );

    return Form(
      key: _formKey,
      child: Mutation(
      options: MutationOptions(
          documentNode: gql(userMutations.signUpMutation),
          update: (Cache cache, QueryResult res) async {
            if (!res.hasException && res.data != null) {
              bool loggedInSuccessfully =
                  await Provider.of<UserService>(context)
                      .attemptLoginAfterSignup(_email, _password);

              if (loggedInSuccessfully) {
                Navigator.pushNamed(context, routes.tasks);
                _formKey.currentState.reset();
              } else {
                errorDialog(context,
                    'Something went wrong trying to authenticate, please try again.');
              }
            } else {
              errorDialog(
                  context, res.exception?.graphqlErrors?.first?.message);
              /* Do not cache on error - might be a bug from the zino-app package */
              cache.reset();
            }
          },
          onCompleted: (dynamic resultData) {
            if (resultData != null) {
              final String username = resultData['signUp']['name'];

              Provider.of<UserService>(context).updateUsername(username);
            }
          }),
      builder: (RunMutation runMutation, QueryResult queryResult) => Scaffold(
        body: Builder(builder: (context) {
          return SingleChildScrollView(
            child: Container(
              height: MediaQuery.of(context).size.height,
              padding: EdgeInsets.only(
                top: 72.0,
              ),
              decoration: BoxDecoration(
                color: Colors.white,
              ),
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Container(
                          padding: EdgeInsets.only(right: 40.0),
                          child: PagBackButton())
                    ],
                    mainAxisAlignment: MainAxisAlignment.end,
                  ),
                  PagDivider(height: 40.0),
                  InputFieldLabel(labelTitle: 'EMAIL'),
                  PagTextField(
                      hintText: 'email@provider.com',
                      validateFunction: (val) =>
                          _validationsService.validateEmail(val),
                      saveFunction: (val) => _email = val,),
                  PagDivider(),

                  InputFieldLabel(labelTitle: 'NAME'),
                  PagTextField(
                      hintText: 'Your name here',
                      validateFunction: (val) =>
                          _validationsService.validateUsername(val),
                      saveFunction: (val) => _username = val,),
                  PagDivider(),

                  InputFieldLabel(labelTitle: 'PASSWORD'),
                  PagTextField(
                      hintText: '••••••••',
                      isPassword: true,
                      validateFunction: (val) =>
                          _validationsService.validatePassword(val),
                      saveFunction: (val) => _password = val,
                      onChangedFunction: (val) => _password = val,),
                  PagDivider(),

                  InputFieldLabel(labelTitle: 'CONFIRM PASSWORD'),
                  PagTextField(
                      hintText: '••••••••',
                      isPassword: true,
                      validateFunction: (val) =>
                          _validationsService.validateConfirmPassword(val, _password),
                      saveFunction: (val) => _confirmPassword = val,
                      onChangedFunction: (val) => _confirmPassword = val,),
                  PagDivider(),

                  RegisterButton(
                      displayError: displaySnackbarError,
                      onPressedFunction: () {
                        final _form = _formKey.currentState;
                          if (_form.validate()) {
                            _form.save();

                            runMutation({
                              'email': _email,
                              'name': _username,
                              'password': _password
                            });
                          }
                      }),
                ],
              ),
            ),
          );
        }),
      ),
    ),
    );
  }
}
