import 'package:flutter/material.dart';
import 'package:flutter_pagtodo/services/user_service.dart';
import 'package:flutter_pagtodo/services/task_service.dart';
import 'package:provider/provider.dart';
import '../widgets/init_wrapper.dart';

/* ui specific widgets */
import '../widgets/tasks/tasks_upper_header.dart';
import '../widgets/tasks/tasks_lower_header.dart';
import '../widgets/tasks/tasks_list.dart';

/* bottom sheet modal */
import './add_task_screen.dart';

class TasksScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return InitWrapper(
        onInit: () {
          Provider.of<TaskService>(context).populateTasks();
        },
        child: WillPopScope(
          /* Disable androids native back button */
          onWillPop: () => Future.value(false),
          child: Scaffold(
            backgroundColor: Color(0xff002c47),
            floatingActionButton: Padding(
              padding: EdgeInsets.only(right: 6.0),
              child: FloatingActionButton(
                backgroundColor: Color(0xff002c47),
                child: Icon(Icons.add),
                onPressed: () {
                  showModalBottomSheet(
                    context: context,
                    builder: (context) => AddTaskScreen(),
                  );
                },
              ),
            ),
            body: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                TasksUpperHeader(),
                TasksLowerHeader(
                  username: Provider.of<UserService>(context).username,
                  taskCount: Provider.of<TaskService>(context).taskCount
                ),
                Padding(
                  padding: EdgeInsets.only(left: 16.0, bottom: 4.0),
                  child: Text(
                    'to delete press and hold at the desired task title',
                    style: TextStyle(
                      fontSize: 12.0,
                      fontStyle: FontStyle.italic,
                      color: Colors.white,
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    padding: EdgeInsets.only(left: 16.0, top: 16.0),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20.0),
                        topRight: Radius.circular(20.0),
                      ),
                    ),
                    child: TasksList(),
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
