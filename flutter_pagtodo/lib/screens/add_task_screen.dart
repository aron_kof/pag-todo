import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../services/task_service.dart';
import '../services/validations_service.dart';
import '../widgets/input_field_label.dart';
import '../widgets/pag_text_field.dart';

class AddTaskScreen extends StatefulWidget {
  @override
  AddTaskScreenState createState() => new AddTaskScreenState();
}

class AddTaskScreenState extends State<AddTaskScreen> {
  final _formKey = new GlobalKey<FormState>();
  final _validationsService = ValidationsService();

  String _taskName;

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          color: Color(0xff757575),
          child: Container(
            padding: EdgeInsets.all(20.0),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(20.0),
                topRight: Radius.circular(20.0),
              ),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                InputFieldLabel(
                  labelTitle: 'Task title',
                ),
                PagTextField(
                    hintText: 'Insert title here',
                    autofocus: true,
                    validateFunction: (val) =>
                        _validationsService.validateTaskName(val),
                    saveFunction: (val) => _taskName = val,),
                Container(
                  width: MediaQuery.of(context).size.width,
                  margin: const EdgeInsets.only(top: 24.0),
                  alignment: Alignment.center,
                  child: FlatButton(
                    child: Text(
                      'Add',
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                    color: Color(0xff002c47),
                    onPressed: () {
                      final _form = _formKey.currentState;

                      if (_form.validate()) {
                        _form.save();
                        Provider.of<TaskService>(context)
                            .addNewTask(_taskName);
                        Navigator.pop(context);
                      }
                    },
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
