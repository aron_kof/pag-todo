class LoginScreenArguments {
  final bool authError = false;
  final String message;

  LoginScreenArguments([this.message]);
}