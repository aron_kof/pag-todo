import 'package:flutter/material.dart';
import 'package:flutter_pagtodo/services/routes.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_pagtodo/screens/login_screen_arguments.dart';
import 'package:flutter_pagtodo/services/user_service.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import '../gql_operations/user_operations.dart';
import 'package:provider/provider.dart';
import '../services/validations_service.dart';

/* ui widgets import */
import '../widgets/pag_logo.dart';
import '../widgets/input_field_label.dart';
import '../widgets/pag_text_field.dart';
import '../widgets/pag_divider.dart';
import '../widgets/pag_text_divider.dart';

/* specific widgets import */
import '../widgets/login/login_button.dart';
import '../widgets/login/signup_button.dart';

class LoginScreen extends StatefulWidget {
  @override
  LoginScreenState createState() => new LoginScreenState();
}

class LoginScreenState extends State<LoginScreen> {
  final _formKey = new GlobalKey<FormState>();
  final _validationsService = ValidationsService();

  String _email;
  String _password;

  @override
  Widget build(BuildContext context) {
    Routes routes = Routes();
    UserMutations userMutations = UserMutations();

    final LoginScreenArguments arguments =
        ModalRoute.of(context).settings.arguments;

    SnackBar createSnackBar([String optionalMessage]) => SnackBar(
          content: Text(
              optionalMessage ??
                  'Oops, something went wrong. Please, try again.',
              textAlign: TextAlign.center),
        );

    void displaySnackbarError(BuildContext context, [String optionalMessage]) {
      Scaffold.of(context).showSnackBar(createSnackBar(optionalMessage));
    }

    void errorDialog(BuildContext context, [String errorMessage]) => showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Error'),
              content: Text(errorMessage ??
                  'Oops, something went wrong, try again please.'),
              actions: [
                FlatButton(
                    child: Text("OK"),
                    onPressed: () {
                      Navigator.pop(context);
                    })
              ],
            );
          },
        );

    return Form(
      key: _formKey,
      child: Mutation(
        options: MutationOptions(
            documentNode: gql(userMutations.signInMutation),
            update: (Cache cache, QueryResult res) {
              if (!res.hasException && res.data != null) {
                Navigator.pushNamed(context, routes.tasks);
                _formKey.currentState.reset();
              } else {
                errorDialog(
                    context, res.exception?.graphqlErrors?.first?.message);
                /* Do not cache on error - might be a bug from the zino-app package */
                cache.reset();
              }
            },
            onCompleted: (dynamic resultData) {
              if (resultData != null) {
                final String username = resultData['signIn']['name'];

                Provider.of<UserService>(context).updateUsername(username);
              }
            }),
        builder: (RunMutation runMutation, QueryResult queryResult) => Scaffold(
          body: Builder(builder: (context) {
            SchedulerBinding.instance.addPostFrameCallback((_) async {
              if (arguments != null && arguments.authError) {
                displaySnackbarError(context, arguments.message);
              }
            });

            return new SingleChildScrollView(
              child: Container(
                height: MediaQuery.of(context).size.height,
                decoration: BoxDecoration(
                  color: Colors.white,
                ),
                child: new Column(
                  children: <Widget>[
                    PagLogo(
                      marginTop: 80.0,
                      marginBottom: 40.0,
                    ),
                    InputFieldLabel(labelTitle: 'EMAIL'),
                    PagTextField(
                      hintText: 'email@provider.com',
                      validateFunction: (val) =>
                          _validationsService.validateEmail(val),
                      saveFunction: (val) => _email = val,
                    ),
                    PagDivider(),
                    InputFieldLabel(labelTitle: 'PASSWORD'),
                    PagTextField(
                        hintText: '••••••••',
                        isPassword: true,
                        validateFunction: (val) =>
                          _validationsService.validateLoginPassword(val),
                        saveFunction: (val) => _password = val),
                    PagDivider(),
                    LoginButton(
                        displayError: displaySnackbarError,
                        onPressedFunction: () {
                          final _form = _formKey.currentState;
                          if (_form.validate()) {
                            _form.save();
                            runMutation({'email': _email, 'password': _password}); 
                          }
                        }),
                    PagTextDivider(text: 'OR SIGNUP NOW'),
                    SignupButton(
                      onPressedFunction: () {
                        Navigator.pushNamed(context, routes.signup);
                      },
                    )
                  ],
                ),
              ),
            );
          }),
        ),
      ),
    );
  }
}
