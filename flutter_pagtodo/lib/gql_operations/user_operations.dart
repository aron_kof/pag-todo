class UserQueries {
  String currentUser = """
    query CurrentUser() {
      currentUser {
        id
        email
        name
        createdAt
      }
    }
  """;
}

class UserMutations {
  String signUpMutation = """
    mutation SignUpMutation(\$email: String!, \$name: String!, \$password: String!) {
      signUp(email: \$email, name: \$name, password: \$password) {
        id
        email
        name
        createdAt
      }
    }
  """;

  String signInMutation = """
    mutation SignInMutation(\$email: String!, \$password: String!) {
      signIn(email: \$email, password: \$password) {
        id
        email
        name
        createdAt
      }
    }
  """;

  String signOutMutation = """
    mutation SignOutMutation() {
      signOut
    }
  """;
}