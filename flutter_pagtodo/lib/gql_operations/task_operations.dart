class TaskQueries {
  String getTasksQuery = """
    query GetTasksQuery {
      tasks {
        id
        name
        userId
        completed
        createdAt
      }
    }
  """;
}

class TaskMutations {
  String addTaskMutation = """
    mutation AddTaskMutation(\$name: String!) {
      addTask(name: \$name) {
        id
        name
        userId
        completed
        createdAt
      }
    }
  """;

  String markTaskAsIncompleteMutation = """
    mutation MarkTaskAsIncompleteMutation(\$taskId: String!) {
      markAsIncomplete(taskId: \$taskId) {
        id
        name
        userId
        completed
        createdAt
      }
    }
  """;

  String completeTaskMutation = """
    mutation CompleteTaskMutation(\$taskId: String!) {
      completeTask(taskId: \$taskId) {
        id
        name
        userId
        completed
        createdAt
      }
    }
  """;

  String removeTaskMutation = """
    mutation RemoveTaskMutation(\$taskId: String!) {
      removeTask(taskId: \$taskId)
    }
  """;
}