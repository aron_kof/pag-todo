import 'package:flutter/foundation.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:flutter_pagtodo/gql_operations/user_operations.dart';
import '../gqlConfig.dart';

class UserService extends ChangeNotifier {
  final UserMutations userMutations = UserMutations();

  static GqlConfig _gqlConfig = GqlConfig();
  final GraphQLClient _gqlClient = _gqlConfig.getClientInstance();

  String _username;
  String get username => _username;

  void updateUsername(String newUsername) {
     _username = newUsername;
    notifyListeners();
  }

  Future<bool> attemptLoginAfterSignup(String _email, String _password) async {
    QueryResult res = await _gqlClient.mutate(MutationOptions(
      documentNode: gql(userMutations.signInMutation),
      variables: { 'email': _email, 'password': _password },
      onCompleted: (dynamic resultData) {
        if (resultData != null) {
          this.updateUsername(resultData['signIn']['name']);
        }
      }
    ));

    return !res.hasException && res.data != null;
  }
}
