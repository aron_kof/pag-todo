import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'dart:collection';
import '../models/task.dart';
import 'package:flutter_pagtodo/gqlConfig.dart';
import '../gql_operations/task_operations.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import './animation_service.dart';

class TaskService extends ChangeNotifier {
  final TaskQueries taskQueries = TaskQueries();
  final TaskMutations taskMutations = TaskMutations();

  static GqlConfig _gqlConfig = GqlConfig();
  final GraphQLClient _gqlClient = _gqlConfig.getClientInstance();

  final TasksAnimation _animations = TasksAnimation();

  final int _staggerDuration = 100; // ms between each iteration of animation

  List<Task> _tasks = [];

  bool _initialized = false;
  bool get initialized => _initialized;

  UnmodifiableListView<Task> get tasks {
    return UnmodifiableListView(_tasks);
  }

  int get taskCount {
    return _tasks.length;
  }

  void populateTasks () async {
    QueryResult result = await _gqlClient.query(
      QueryOptions(
        documentNode: gql(taskQueries.getTasksQuery),
        fetchPolicy: FetchPolicy.networkOnly
      ),
    );

    final fetchedTasks = result.data['tasks'];

    /* i'm glad a void method can have early return... */
    if (fetchedTasks.length == 0) {
      _initialized = true;
      notifyListeners();
      return;
    }

    for (var task in fetchedTasks) {
      await Future.delayed(Duration(milliseconds: _staggerDuration), () async {
        _tasks.add(Task.fromJson(task));
        notifyListeners();

        _animations.taskList?.currentState?.insertItem(_tasks.length - 1);

        if(!_initialized) _initialized = true;
      });
    }
  }

  void addNewTask(String taskName) {
    _gqlClient.mutate(MutationOptions(
      documentNode: gql(taskMutations.addTaskMutation),
      variables: { 'name': taskName },
      update: (Cache cache, QueryResult res) {
        if (!res.hasException && res.data != null) {
          // review - do we need any action here [ ? ]
        } else {
          /* TODO - implement error handling */
          cache.reset();
        }
      },
      onCompleted: (dynamic resultData) {
        if (resultData != null) {
          _tasks.insert(0, Task.fromJson(resultData['addTask']));
          _animations.taskList?.currentState?.insertItem(0);
          notifyListeners();
        }
      }
    ));
  }

  void updateTask(Task task) async {
    final String mutation = task.completed
      ? taskMutations.markTaskAsIncompleteMutation
      : taskMutations.completeTaskMutation;

    /* manual optimistic UI implementation */
    task.completed = !task.completed;
    notifyListeners();

    _gqlClient.mutate(MutationOptions(
      documentNode: gql(mutation),
      variables: { 'taskId': task.id },
      update: (Cache cache, QueryResult res) {
        if (res.hasException || res.data == null) {
          /* if there's an exception or data is null - apply rollback */
          task.completed = !task.completed;
          notifyListeners();
          cache.reset();
        }
      }
    ));
  }

  void deleteTask(Task task, int index) async {
    _tasks.remove(task);
    notifyListeners();

    _gqlClient.mutate(MutationOptions(
      documentNode: gql(taskMutations.removeTaskMutation),
      variables: { 'taskId': task.id },
      update: (Cache cache, QueryResult res) {
        if (res.hasException || res.data == null) {
          /* if there's an exception or data is null - apply rollback */
          _tasks.insert(index, task);
          notifyListeners();

          _animations.taskList.currentState.insertItem(index);
          cache.reset();
        }
      }
    ));
  }

  void resetState() {
    _initialized = false;
    _tasks.clear();
    _gqlClient.cache.reset();
  }
}
