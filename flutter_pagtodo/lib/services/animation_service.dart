import 'package:flutter/material.dart';

class TasksAnimation {
  TasksAnimation._internal();
  factory TasksAnimation() => _singleton;
  static final TasksAnimation _singleton = TasksAnimation._internal();

  GlobalKey<AnimatedListState> taskList = GlobalKey();
}