class PagSession {
  PagSession._internal();
  factory PagSession() => _singleton;
  static final PagSession _singleton = PagSession._internal();

  String cookie = '';

  void clearCookie() { cookie = ''; }
}