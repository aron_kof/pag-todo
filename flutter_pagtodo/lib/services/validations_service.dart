class ValidationsService {
  String validateEmail(String val) {
    RegExp emailRegex = RegExp(
      r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$",
      caseSensitive: false,
      multiLine: false
    );


    if (val == null || !emailRegex.hasMatch(val)) {
      return 'Invalid email';
    }

    return null;
  }

  String validateLoginPassword(String val) {
    if (val == null) {
      return 'Password is required';
    }

    if (val.length < 8) {
      return 'Invalid password, must be at least 8 chars. long';
    }

    return null;
  }

  String validatePassword(String val) {
    RegExp passwordRegex = new RegExp(
      r"^(?=\S*[a-z])(?=\S*[A-Z])(?=\S*\d)(?=\S*[^\w\s])\S{8,30}$",
      caseSensitive: true,
      multiLine: false,
    );

    if (!passwordRegex.hasMatch(val)) {
      return 'Must have at least 8 chars., capital letters and special chars.';
    }

    return null;
  }

  String validateConfirmPassword(String val, String matchingPassword) {
    if (val != matchingPassword) {
      return 'Passwords do not match, please try again';
    }

    return null;
  }

  String validateUsername(String val) {
    if (val.length < 3) {
      return 'Please, insert a valid username';
    }

    return null;
  }

  String validateTaskName(String val) {
    if (val.length < 3) {
      return 'Please, insert a valid task name';
    }

    return null;
  }
}