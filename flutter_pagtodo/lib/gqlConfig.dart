import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:flutter/foundation.dart';
import './services/session_service.dart';
import 'dart:io';

import 'package:http/src/base_request.dart';
import 'package:http/src/io_client.dart';
import 'package:http/src/streamed_response.dart';

class CookiesCatcherClient extends IOClient {
  PagSession _session = PagSession();

  @override
  Future<StreamedResponse> send(BaseRequest request) async {
    request.headers['cookie'] = _session.cookie;
    return super.send(request).then((response) {
      _session.cookie = response.headers['set-cookie'] ?? '';
      return response;
    });
  }
}

class GqlConfig {
  static String _uri = Platform.isAndroid
      ? 'http://10.0.2.2:4000/graphql'
      : 'http://localhost:4000/graphql';

  HttpLink _createHttpLink() {
    return new HttpLink(
      uri: _uri,
      httpClient: CookiesCatcherClient()
    );
  }

  GraphQLClient getClientInstance() {
    return GraphQLClient(
      cache: InMemoryCache(),
      link: _createHttpLink()
    );
  }

  ValueNotifier<GraphQLClient> buildClient([bool resetSession]) {
    return ValueNotifier<GraphQLClient>(
      GraphQLClient(
        cache: InMemoryCache(),
        link: _createHttpLink()
      )
    );
  }
}