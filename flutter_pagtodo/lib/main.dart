import 'package:flutter/material.dart';
import 'package:flutter_pagtodo/gqlConfig.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:flutter_pagtodo/services/routes.dart';
import 'package:flutter_pagtodo/services/user_service.dart';
import 'package:flutter_pagtodo/services/task_service.dart';
import 'package:provider/provider.dart';

import 'screens/login_screen.dart';
import 'screens/signup_screen.dart';
import 'screens/tasks_screen.dart';

/*
pag colors
  Color(0xff002c47) Primary
  Color(0xff63d2ff) Secondary
*/

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Routes routes = Routes();
    GqlConfig gqlConfig = GqlConfig();

    return GraphQLProvider(
        client: gqlConfig.buildClient(),
        child: MultiProvider(
          /* There's no problem using both providers for the whole
             application since it is so small */
          providers: [
            ChangeNotifierProvider(create: (context) => UserService(),),
            ChangeNotifierProvider(create: (context) => TaskService(),)
          ],
          child: MaterialApp(
            title: 'Flutter PagTodo',
            initialRoute: 'login',
            routes: {
              routes.login: (context) => LoginScreen(),
              routes.tasks: (context) => TasksScreen(),
              routes.signup: (context) => SignupScreen(),
            },
          ),
        ),
    );
  }
}