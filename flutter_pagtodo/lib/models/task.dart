class Task {
  String id;
  String name;
  String userId;
  bool completed;

  Task({this.id, this.name, this.completed = false, this.userId});

  factory Task.fromJson(Map<String, dynamic> jsonTask) {
    return Task(
      id: jsonTask['id'],
      name: jsonTask['name'],
      completed: jsonTask['completed'],
      userId: jsonTask['userId'],
    );
  }

  void toggleCompletedState() {
    completed = !completed;
  }
}
