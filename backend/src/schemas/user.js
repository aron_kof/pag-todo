import Joi from 'joi';

const userKeys = {
  email: Joi.string().email().required().label('Email'),
  name: Joi.string().min(3).max(40).required().label('Name'),
  password: Joi.string().regex(/^(?=\S*[a-z])(?=\S*[A-Z])(?=\S*\d)(?=\S*[^\w\s])\S{8,30}$/).label('Password')
  .options({
    language: {
      string: {
        regex: {
          base: 'is invalid'
        }
      }
    }
  })
};

export const signUp = Joi.object().keys(userKeys);

export const signIn = Joi.object().keys({
  email: userKeys.email,
  password: userKeys.password
});
