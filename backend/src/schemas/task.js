import Joi from 'joi';

const taskKeys = {
  name: Joi.string().max(300).required().label('Name'),
  completed: Joi.boolean().required().label('Price'),
  userId: Joi.string().required().label('User Id')
};

export const createTask = Joi.object().keys(taskKeys);