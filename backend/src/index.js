import { ApolloServer } from 'apollo-server-express';

import express from 'express';
import session from 'express-session';
import connectRedis from 'connect-redis';

import mongoose from 'mongoose';

import typeDefs from './typeDefinitions';
import schemaDirectives from './schemaDirectives';
import resolvers from './resolvers';

import {
  APP_PORT,
  DB_HOST,
  DB_USERNAME,
  DB_PASSWORD,
  DB_PORT,
  DB_NAME,
  IS_PRODUCTION,
  SESSION_NAME,
  SESSION_SECRET,
  SESSION_LIFETIME,
  REDIS_HOST,
  REDIS_PORT,
  REDIS_PASSWORD
} from './config';

(async () => {
  try {
    const connectionString = `mongodb://${DB_USERNAME}:${DB_PASSWORD}@${DB_HOST}:${DB_PORT}/${DB_NAME}`;

    await mongoose.connect(connectionString, { useNewUrlParser: true });

    const app = express();
    app.disable('x-powered-by');

    const RedisStore = connectRedis(session);

    const store = new RedisStore({
      host: REDIS_HOST,
      port: REDIS_PORT,
      password: REDIS_PASSWORD
    });

    app.use(session({
      store,

      name: SESSION_NAME,
      secret: SESSION_SECRET,

      /* This will renew the session lifetime whenever a request is made */
      resave: true,
      rolling: true,

      saveUninitialized: false,
      cookie: {
        maxAge: parseInt(SESSION_LIFETIME, 10),
        sameSite: true,
        secure: IS_PRODUCTION
      }
    }));

    const server = new ApolloServer({
      typeDefs,
      resolvers,
      schemaDirectives,
      playground: IS_PRODUCTION
        ? false
        : { settings: { 'request.credentials': 'include' } },
      context: ({ req, res }) => ({ req, res })
    });

    server.applyMiddleware({
      app,
      cors: { credentials: true, origin: 'http://localhost:4000' }
    });

    app.listen({ port: APP_PORT }, () => {
      console.log(`PAG - TODO LIST Server ready at http://localhost:${APP_PORT}${server.graphqlPath} 🚀🚀🚀`);
    });
  } catch (e) {
    console.error(e);
  }
})();