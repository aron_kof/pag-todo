import Joi from 'joi';
import mongoose from 'mongoose';
import { UserInputError } from 'apollo-server-express';
import { User } from '../models';
import { signUp, signIn } from '../schemas';
import { auth } from '../services';

export default {
  Query: {
    currentUser: (root, args, { req }, info) => {
      return User.findById(req.session.userId);
    },
  },

  Mutation: {
    signUp: async (root, args, { req }, info) => {
      const newUser = {
        email: args.email,
        name: args.name,
        password: args.password
      };

      await Joi.validate(newUser, signUp, { abortEarly: false });

      const user = User.create(newUser);

      req.session.userId = user.id;

      return user;
    },

    signIn: async (root, args, { req }, info) => {
      await Joi.validate(args, signIn, { abortEarly: false });

      const user = await auth.attemptSignIn(args.email, args.password);

      req.session.userId = user.id;

      return user;
    },

    signOut: async (root, args, { req, res }, info) => auth.signOut(req, res)
  }
};
