import Joi from 'joi';
import mongoose from 'mongoose';
import { UserInputError } from 'apollo-server-express';
import { createTask } from '../schemas';
import { Task } from '../models';
import { tupleTypeAnnotation } from '@babel/types';

export default {
  Query: {
    tasks: (root, args, ctx, info) => {
      const { userId } = ctx.req.session;

      return Task.find({ userId });
    }
  },

  Mutation: {
    addTask: async (root, args, ctx, info) => {
      const { userId } = ctx.req.session;

      const newTask = {
        name: args.name,
        completed: false,
        userId
      };

      await Joi.validate(newTask, createTask, { abortEarly: false });

      return Task.create(newTask);
    },

    completeTask: async (root, { taskId }, ctx, info) => {
      const { userId } = ctx.req.session;
      const task = await Task.findById(taskId);

      task.validateOwnership(userId);

      return task.completeTask();
    },

    markAsIncomplete: async (root, { taskId }, ctx, info) => {
      const { userId } = ctx.req.session;
      const task = await Task.findById(taskId);

      task.validateOwnership(userId);

      return task.markAsIncomplete();
    },
    
    removeTask: async (root, { taskId }, ctx, info) => {
      const { userId } = ctx.req.session;
      const task = await Task.findById(taskId);
      
      task.validateOwnership(userId);

      try {
        await task.remove(err => !err);
        return true;
      } catch(err) {
        console.log(err);
        return false;
      }
    },
  }
};
