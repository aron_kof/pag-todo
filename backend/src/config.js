export const {
  /* express config */
  APP_PORT = 4000,
  NODE_ENV = 'development',

  /* express session config */
  SESSION_NAME = 'sid',
  SESSION_SECRET = '<super-secret>',
  SESSION_LIFETIME = 1000 * 60 * 60 * 2, /* 2 hours lifetime */

  /* mongodb config */
  DB_HOST = 'ds349628.mlab.com',
  DB_USERNAME = 'admin',
  DB_PASSWORD = 'pag12345',
  DB_PORT = 49628,
  DB_NAME = 'pag-todo',
  RUN_TESTS = 0,

  /* redis config */
  REDIS_HOST = 'redis-13140.c85.us-east-1-2.ec2.cloud.redislabs.com',
  REDIS_PORT = 13140,
  REDIS_PASSWORD = '1ygN0kgAeOVKukznnFrqTMLyhpeWnUNe'
} = process.env;

export const IS_PRODUCTION = NODE_ENV === 'production';