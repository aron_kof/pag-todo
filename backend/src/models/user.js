import mongoose from 'mongoose';
import { hash, compare } from 'bcryptjs';

const userSchema = new mongoose.Schema({
  email: {
    type: String,
    validate: {
      validator: email => User.doesntExist({ email }),
      msg: () => 'Email has already been taken.'
    }
  },
  username: {
    type: String,
    validate: {
      validator: username => User.doesntExist({ username }),
      msg: () => 'Username has already been taken.'
    }
  },
  password: String,
  name: String
}, {
  timestamps: true
});

userSchema.pre('save', async function () {
  if (this.isModified('password')) {
    this.password = await hash(this.password, 12);
  }
});

userSchema.methods.checkPassword = function (password) {
  return compare(password, this.password);
};

userSchema.statics.doesntExist = async function (condition) {
  return await this.find(condition).countDocuments() === 0;
};

userSchema.statics.isInitialized = async function () {
  return await this.find().countDocuments() > 0;
};

const User = mongoose.model('User', userSchema);

export default User;
