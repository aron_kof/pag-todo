import { ForbiddenError } from 'apollo-server-express';
import mongoose from 'mongoose';

const taskSchema = new mongoose.Schema({
  name: String,
  completed: Boolean,
  userId: String
}, {
  timestamps: true
});

taskSchema.indexes({ userId: 'text' });

taskSchema.methods.validateOwnership = function(loggedInUserId) {
  if (!loggedInUserId || this.userId !== loggedInUserId) {
    throw new ForbiddenError('This action can only be performed by the owner of the task.');
  }
};

taskSchema.methods.completeTask = async function() {
  this.completed = true;

  await this.save();

  return this;
};

taskSchema.methods.markAsIncomplete = async function() {
  this.completed = false;

  await this.save();

  return this;
};

const Task = mongoose.model('Task', taskSchema);

export default Task;
