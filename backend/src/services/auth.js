import { AuthenticationError } from 'apollo-server-express';
import { SESSION_NAME } from '../config';
import { User } from '../models';

const signedIn = req => req.session.userId;

export default {
  ensureLoggedIn: (req) => {
    if (!signedIn(req)) {
      throw new AuthenticationError('You must be signed in.');
    }
  },

  ensureLoggedOut: (req) => {
    if (req.session.userId) {
      throw new AuthenticationError('You are already signed in.');
    }
  },

  attemptSignIn: async (email, password) => {
    const genericErrorMsg = 'Incorrect email or password. Please try again.';
    const user = await User.findOne({ email });

    if (!user) {
      throw new AuthenticationError(genericErrorMsg);
    }

    if (!await user.checkPassword(password)) {
      throw new AuthenticationError(genericErrorMsg);
    }

    return user;
  },

  signOut: (req, res) => new Promise((resolve, reject) => {
    req.session.destroy((err) => {
      if (err) {
        reject(err);
      }

      res.clearCookie(SESSION_NAME);

      resolve(true);
    });
  })
};
