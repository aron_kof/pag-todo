import { SchemaDirectiveVisitor } from 'apollo-server-express';
import { defaultFieldResolver } from 'graphql';
import { auth } from '../services';

class Authorize extends SchemaDirectiveVisitor {
  visitFieldDefinition(field) {
    const { resolve = defaultFieldResolver } = field;

    field.resolve = async function (...args) {
      const [,, context] = args;

      await auth.ensureLoggedIn(context.req);

      return resolve.apply(this, args);
    };
  }
}

export default Authorize;
