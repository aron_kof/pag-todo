import { SchemaDirectiveVisitor } from 'apollo-server-express';
import { defaultFieldResolver } from 'graphql';
import { auth } from '../services';

class Guest extends SchemaDirectiveVisitor {
  /* field arg. would be any Query/Mutation field (i.e: signIn mutation field) */
  visitFieldDefinition(field) {
    const { resolve = defaultFieldResolver } = field;

    field.resolve = async function (...args) {
      /* destructuring 3rd argument from resolve fns (ctx) */
      const [,, context] = args;

      await auth.ensureLoggedOut(context.req);

      return resolve.apply(this, args);
    };
  }
}

export default Guest;