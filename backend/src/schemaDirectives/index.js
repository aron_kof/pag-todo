import Authorize from './authorize';
import Guest from './guest';

export default {
  authorize: Authorize,
  guest: Guest
};