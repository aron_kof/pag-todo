import express from 'express';
import session from 'express-session';
import connectRedis from 'connect-redis';

import {
  IS_PRODUCTION,
  SESSION_NAME,
  SESSION_SECRET,
  SESSION_LIFETIME,
  REDIS_HOST,
  REDIS_PORT,
  REDIS_PASSWORD
} from '../config';

console.log('### SETTING UP MINIMAL EXPRESS APP FOR SESSION TEST ###');

const app = express();
app.disable('x-powered-by');

const RedisStore = connectRedis(session);

const store = new RedisStore({
  host: REDIS_HOST,
  port: REDIS_PORT,
  password: REDIS_PASSWORD
});

app.use(session({
  store,

  name: SESSION_NAME,
  secret: SESSION_SECRET,

  /* This will renew the session lifetime whenever a request is made */
  resave: true,
  rolling: true,

  saveUninitialized: false,
  cookie: {
    maxAge: parseInt(SESSION_LIFETIME, 10),
    sameSite: true,
    secure: IS_PRODUCTION
  }
}));

describe('Redis connection and express session strategy setup', () => {
  it('should contain the session property on the request object', () => {
    app.use(function(req) {
      expect(req).toBeDefined();
    })
  });
});