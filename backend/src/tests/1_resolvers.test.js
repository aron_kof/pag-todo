import mongoose from 'mongoose';
import { gqlRequest } from './utils/gqlRequest';
import createMongoTestConnection from './utils/mongo-test-connection';
import { Task, User } from '../models';
import { mockUser1, mockUser2 } from './utils/mockups';

import {
  signInMutation,
  registerMutation,
  addTaskMutation,
  getTasksQuery,
  completeTaskMutation,
  markTaskAsIncompleteMutation,
  removeTaskMutation
} from './utils/query-mutations';

import { _ } from 'core-js';

beforeAll(async () => {
  await createMongoTestConnection();
});

afterAll(async () => {
  console.log('### DROP USER AND TASK COLLECTIONS AFTER TESTS COMPLETION ###');
  await User.collection.drop();
  await Task.collection.drop();
  
  await mongoose.connection.close();
});

let _mockupUser1 = {};
let _mockupUser2 = {};

describe('Resolvers tests', () => {
  it('should register mockup users with success', async () => {
    const mockup1Response = await gqlRequest(registerMutation, mockUser1);

    _mockupUser1 = mockup1Response.data.signUp;
    mockUser1.id = _mockupUser1.id;

    expect(_mockupUser1).toBeDefined();
    expect(_mockupUser1.id).toBeTruthy();

    const mockup2Response = await gqlRequest(registerMutation, mockUser2);

    _mockupUser2 = mockup2Response.data.signUp;
    mockUser2.id = _mockupUser2.id;

    expect(_mockupUser2).toBeDefined();
    expect(_mockupUser2.id).toBeTruthy();
  });

  it('should signin with success with valid credentials', async () => {
    const loginResponse = await gqlRequest(signInMutation, { email: mockUser1.email, password: mockUser1.password });

    expect(loginResponse.data.signIn.id).toBeTruthy();
  });

  it('should create two tasks for each mockup user with success', async () => {
    const createTasks = async (...mockUsers) => {
      while (mockUsers.length) {
        const mockUser = mockUsers[mockUsers.length - 1];

        for (let { name } of mockUser.tasks) {
          const taskResponse = await gqlRequest(addTaskMutation, { name }, mockUser.id);
    
          expect(taskResponse.data.addTask.id).toBeTruthy();
        }

        mockUsers.pop();
      }
    };

    await createTasks(mockUser1, mockUser2);
  });

  it('should bring the list of tasks of the specified user', async () => {
    const specifiedUser = mockUser1;

    const tasksQueryResponse = await gqlRequest(getTasksQuery, {}, specifiedUser.id);

    expect(tasksQueryResponse.data.tasks).toHaveLength(2);

    const [task1, task2] = tasksQueryResponse.data.tasks;

    /* not 100% sure this will work evert time - considering to change it toBeTruthy with or operator for both names */
    expect(task1.name).toBe(mockUser1.tasks[0].name);
    expect(task2.name).toBe(mockUser1.tasks[1].name);
  });

  it('should not be able to retrieve the tasks(no session.userId)', async () => {
    const tasksQueryResponse = await gqlRequest(getTasksQuery, {}, null);

    expect(tasksQueryResponse.errors).toBeTruthy();
    expect(tasksQueryResponse.data.tasks).toBeNull();
    expect(tasksQueryResponse.errors[0].extensions.code.toLowerCase()).toBe('unauthenticated');
  });

  it('should complete the tasks of the specified user', async () => {
    const specifiedUser = mockUser1;
    const tasksQueryResponse = await gqlRequest(getTasksQuery, {}, specifiedUser.id);
    expect(tasksQueryResponse.data.tasks).toHaveLength(2);

    const [task1, task2] = tasksQueryResponse.data.tasks;

    const completeTask1Response = await gqlRequest(completeTaskMutation, { taskId: task1.id }, specifiedUser.id);
    const completeTask2Response = await gqlRequest(completeTaskMutation, { taskId: task2.id }, specifiedUser.id);

    expect(completeTask1Response.data.completeTask.completed).toBeTruthy();
    expect(completeTask2Response.data.completeTask.completed).toBeTruthy();
  });

  it('should not complete a task that does not belong to the specified user (different userId - session vs. task.userId)', async () => {
    const specifiedUser = mockUser2;
    const differentUser = mockUser1;

    const tasksQueryResponse = await gqlRequest(getTasksQuery, {}, specifiedUser.id);

    const [task] = tasksQueryResponse.data.tasks;

    const completeTaskResponse = await gqlRequest(completeTaskMutation, { taskId: task.id }, differentUser.id);

    expect(completeTaskResponse.errors).toBeTruthy();
    expect(completeTaskResponse.errors[0].extensions.code.toLowerCase()).toBe('forbidden');
  });

  it('should mark as incomplete the first task of the specified user', async () => {
    const specifiedUser = mockUser1;
    const tasksQueryResponse = await gqlRequest(getTasksQuery, {}, specifiedUser.id);
    expect(tasksQueryResponse.data.tasks).toHaveLength(2);

    const [task1] = tasksQueryResponse.data.tasks;

    const incompleteTask1Response = await gqlRequest(markTaskAsIncompleteMutation, { taskId: task1.id }, specifiedUser.id);

    expect(incompleteTask1Response.data.markAsIncomplete.completed).toBeFalsy();
  });

  it('should not mark a task as incomplete that does not belong to the specified user', async () => {
    const specifiedUser = mockUser1;
    const differentUser = mockUser2;

    const tasksQueryResponse = await gqlRequest(getTasksQuery, {}, specifiedUser.id);

    const [,task] = tasksQueryResponse.data.tasks; /* grabbing the 2nd task, which is completed */

    const incompleteTaskResponse = await gqlRequest(markTaskAsIncompleteMutation, { taskId: task.id }, differentUser.id);

    expect(incompleteTaskResponse.errors).toBeTruthy();
    expect(incompleteTaskResponse.errors[0].extensions.code.toLowerCase()).toBe('forbidden');
  });

  it('should delete a task successfully', async() => {
    const specifiedUser = mockUser1;

    const tasksQueryResponse = await gqlRequest(getTasksQuery, {}, specifiedUser.id);

    const [task] = tasksQueryResponse.data.tasks; /* removing first task */

    const removeTaskResponse = await gqlRequest(removeTaskMutation, { taskId: task.id }, specifiedUser.id);

    console.log(removeTaskResponse);

    expect(removeTaskResponse.data).toBeTruthy();

    const tasksQueryResponseAfter = await gqlRequest(getTasksQuery, {}, specifiedUser.id);
    expect(tasksQueryResponseAfter.data.tasks).toHaveLength(1);
  });

  it('should not remove a task that does not belong to the specified user', async () => {
    const specifiedUser = mockUser1;
    const differentUser = mockUser2;

    const tasksQueryResponse = await gqlRequest(getTasksQuery, {}, specifiedUser.id);

    const [task] = tasksQueryResponse.data.tasks; /* grabbing the last task */

    const removeTaskResponse = await gqlRequest(removeTaskMutation, { taskId: task.id }, differentUser.id);

    expect(removeTaskResponse.errors).toBeTruthy();
    expect(removeTaskResponse.errors[0].extensions.code.toLowerCase()).toBe('forbidden');
  });
});