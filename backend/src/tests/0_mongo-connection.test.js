import mongoose from 'mongoose';
import createMongoTestConnection from './utils/mongo-test-connection';
import { User } from '../models';

beforeAll(async () => {
  await createMongoTestConnection();
});

afterAll(async () => {
  await mongoose.connection.close();
});

describe('MongoDB connection and database user permissions', () => {
  it('should insert an User', async () => {
    const mockUser = {
      email: 'pagtest@email.com',
      name: 'pagtest',
      password: 'PagPassword1'
    };

    console.log('### INSERTING MOCK USER ###');
    console.log(mockUser);

    const insertedMockUser = await User.create(mockUser);
    console.log('### LOGGING INSERTED MOCK USER ###');
    console.log(insertedMockUser);

    expect(insertedMockUser).toBeDefined();
    expect(insertedMockUser._id).toBeTruthy();
  });
});