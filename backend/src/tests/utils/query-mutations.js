export const registerMutation = `
  mutation SignUpMutation($email: String!, $name: String!, $password: String!) {
    signUp(email: $email, name: $name, password: $password) {
      id
      email
      name
      createdAt
    }
  }
`;

export const signInMutation = `
  mutation SignInMutation($email: String!, $password: String!) {
    signIn(email: $email, password: $password) {
      id
      email
      name
      createdAt
    }
  }
`;

export const addTaskMutation = `
  mutation AddTaskMutation($name: String!) {
    addTask(name: $name) {
      id
      name
      userId
      completed
      createdAt
    }
  }
`;

export const getTasksQuery = `
  query GetTasksQuery {
    tasks {
      id
      name
      userId
      completed
      createdAt
    }
  }
`;

export const completeTaskMutation = `
  mutation CompleteTaskMutation($taskId: String!) {
    completeTask(taskId: $taskId) {
      id
      name
      userId
      completed
      createdAt
    }
  }
`;

export const markTaskAsIncompleteMutation = `
  mutation MarkTaskAsIncompleteMutation($taskId: String!) {
    markAsIncomplete(taskId: $taskId) {
      id
      name
      userId
      completed
      createdAt
    }
  }
`;

export const removeTaskMutation = `
  mutation RemoveTaskMutation($taskId: String!) {
    removeTask(taskId: $taskId)
  }
`;