export const mockUser1 = {
  email: 'laura.pereira@email.com',
  name: 'Laura Pereira',
  password: 'LauraPassword1-',
  tasks: [
    { name: 'Do the dishes' }, { name: 'Clean my room' }
  ]
};

export const mockUser2 = {
  email: 'aron.koffler@email.com',
  name: 'Aron Koffler',
  password: 'AronPassword2-',
  tasks: [
    { name: '30 mins. run' }, { name: 'Do my daily drawing exercises' }
  ]
};