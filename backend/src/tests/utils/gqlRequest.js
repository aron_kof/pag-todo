import { graphql } from "graphql";
import { makeExecutableSchema } from "graphql-tools";

import typeDefs from "../../typeDefinitions";
import resolvers from "../../resolvers";
import schemaDirectives from '../../schemaDirectives';

const schema = makeExecutableSchema({ typeDefs, resolvers, schemaDirectives });

export const gqlRequest = async (
  query,
  variables,
  userId = null
) => {
  return graphql(
    schema,
    query,
    undefined,
    {
      req: {
        session: {
          userId
        }
      },
      res: {
        clearCookie: () => {}
      }
    },
    variables
  );
};