import mongoose from 'mongoose';

export default async () => {
  const connectionString = 'mongodb://admin:pagtest12345@ds349618.mlab.com:49618/pag-todo-test';
  
  await mongoose.connect(connectionString, { useUnifiedTopology: true, useNewUrlParser: true });
}
