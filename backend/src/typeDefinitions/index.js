import task from './task';
import root from './root';
import user from './user';

export default [
  root,
  task,
  user
];
