import { gql } from 'apollo-server-express';

export default gql`
  extend type Query {
    currentUser: User @authorize
  }

  extend type Mutation {
    signUp(email: String!, name: String!, password: String!): User @guest
    signIn(email: String!, password: String!): User @guest
    signOut: Boolean @authorize
  }

  type User {
    id: ID!
    email: String!
    name: String!
    createdAt: String!
  }
`;
