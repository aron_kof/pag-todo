import { gql } from 'apollo-server-express';

export default gql`
  extend type Query {
    tasks: [Task] @authorize
  }

  extend type Mutation {
    addTask(name: String!): Task @authorize
    completeTask(taskId: String!): Task @authorize
    markAsIncomplete(taskId: String!): Task @authorize
    removeTask(taskId: String!): Boolean! @authorize
  }

  type Task {
    id: ID!
    name: String!
    userId: String!
    completed: Boolean!
    createdAt: String!
  }
`;
