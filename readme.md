# Pag Todo Demo Application

**Frontend application**

* flutter framework
* graphql client integration - zino-app/flutter_graphql
* simple architecture using provider to manage state and singleton patterns as well

**Backend application**

* node, express and apollo server
* validations using Joi library
* schemaDirectives for auth/session management
* redis to control session via cookies
* MongoDB
* very dry code


# How to run it

**pre-req**

First install the dependencies

/flutter_todo

*flutter packages get*

/backend 

*yarn*

**backend tests**

*yarn test*

**running the application**

to run the application in dev env.

/flutter_todo 

*flutter run*

/backend 

*yarn dev*

# Issues and what’s missing for a better app

@animations branch

* Can’t scroll with active keyboard
    * flexible size for content :white_check_mark:
* Validation of mutation inputs is “poor”
    * better validation - validation service facade(maybe using flutter field validators) :white_check_mark:
* Normalize graphql operations design pattern
    * there was a major change in the architecture near the end of the development so the way the graphql operations are done are kind of mixed up 
* Optimistic UI
    * implement optimistic mutations (now being done manually - works ok)
* Erro handling
    * There was no time for proper error handling - there are a lot of gaps, some simple, others critical (like unsuccessful mutations)
    * Friendly error message fix for backend Regex validation
* UI Improvents
    * Implement smoother transitions / animations :white_check_mark:
    * loader and shimmer blocks for loading states
* Forms and input extraction
    * Implementation done as simple as possible - for this application size is fine though :white_check_mark: (COMPLETELY REFACTORED)
* Remaining test scenarios
    * Session lifetime expiration
    * Session expired handler
    * guest schema directive scenarios (useless maybe)
